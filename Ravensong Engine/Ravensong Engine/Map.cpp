#include "Map.h"
#include "Components.h"

#include <fstream>

int Map::mapWidth = 50;
int Map::mapHeight = 30;

Map::Map(const char* path, int scale, int tSize) : tilePath(path), mapScale(scale), tileSize(tSize)
{
	scaledSize = (mapScale * tileSize);
}

Map::~Map()
{}

void Map::LoadMap(std::string path)
{
	char c;
	std::fstream mapFile;
	mapFile.open(path);

	int srcX, srcY;

	for (int y = 0; y < mapHeight; y++)
	{
		for (int x = 0; x < mapWidth; x++)
		{
			mapFile.get(c);
			srcY = atoi(&c) * tileSize;
			mapFile.get(c);
			srcX = atoi(&c) * tileSize;

			AddTile(srcX, srcY, x * scaledSize, y * scaledSize);
			mapFile.ignore();

			tileData[x][y] = new TileData(x, y);
		}
	}

	for (int y = 0; y < mapHeight; y++)
	{
		for (int x = 0; x < mapWidth; x++)
		{
			tileData[x][y]->SetNeighbors(tileData);
		}
	}

	mapFile.close();
}

void Map::AddTile(int srcX, int srcY, int x, int y)
{
	auto& tile(manager.AddEntity());
	tile.AddComponent<TileComponent>(srcX, srcY, x, y, tileSize, tilePath);
	tile.AddToGroup(Game::groupMap);
}

TileData* Map::GetTileAt(int x, int y)
{
	return tileData[x][y];
}

bool Map::RoomAt(int x, int y)
{
	return tileData[x][y]->hasRoom();
}