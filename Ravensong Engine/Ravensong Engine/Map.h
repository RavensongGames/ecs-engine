#pragma once

#include "TileData.h"
#include <string>

class Map
{
public:
	Map(const char* path, int scale, int tSize);
	~Map();

	void LoadMap(std::string path);
	void AddTile(int srcX, int srcY, int x, int y);

	int maxX() { return mapWidth * scaledSize; }
	int maxY() { return mapHeight * scaledSize; }

	TileData* GetTileAt(int x, int y);
	bool RoomAt(int x, int y);

	static int mapWidth;
	static int mapHeight;

private:
	const char* tilePath;
	int mapScale;
	int tileSize;
	int scaledSize;
	TileData* tileData[50][30];
};