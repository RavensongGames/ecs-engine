#pragma once

#include "SDL.h"

class KeyboardController : public Component 
{
public:
	void HandleEvent() override
	{

		keystate = SDL_GetKeyboardState(NULL);

		if (keystate[SDL_SCANCODE_W] || keystate[SDL_SCANCODE_UP])
			MoveCamera(0, -1);
		if (keystate[SDL_SCANCODE_A] || keystate[SDL_SCANCODE_LEFT])
			MoveCamera(-1, 0);
		if (keystate[SDL_SCANCODE_S] || keystate[SDL_SCANCODE_DOWN])
			MoveCamera(0, 1);
		if (keystate[SDL_SCANCODE_D] || keystate[SDL_SCANCODE_RIGHT])
			MoveCamera(1, 0);

		if (Game::event.type == SDL_KEYUP)
		{
			switch (Game::event.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				Game::isRunning = false;
				break;
			default:
				break;
			}
		}
	}

	void MoveCamera(int x, int y)
	{
		Game::camera.x += (x * speed);
		Game::camera.y += (y * speed);
	}

private:
	const Uint8* keystate;
	const int speed = 10;
};