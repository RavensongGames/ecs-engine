#pragma once

#include "SDL.h"
#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <bitset>
#include <array>

class Component;
class Entity;
class Manager;

using ComponentID = std::size_t;
using Group = std::size_t;

inline ComponentID getNewComponentTypeID()
{
	static ComponentID lastID = 0u;
	return lastID++;
}

template <typename T> inline ComponentID getComponentTypeID() noexcept
{
	static ComponentID typeID = getNewComponentTypeID();
	return typeID;
}

constexpr std::size_t maxComponents = 32;
constexpr std::size_t maxGroups = 32;

using ComponentBitSet = std::bitset<maxComponents>;
using GroupBitSet = std::bitset<maxGroups>;
using ComponentArray = std::array<Component*, maxComponents>;


class Component
{
public:
	Entity* entity;
	virtual void Init() {}
	virtual void Update() {}
	virtual void Draw() {}
	virtual void HandleEvent() {}
	virtual ~Component() {}
};


class Entity
{
public:
	Entity(Manager& mManager) : manager(mManager)
	{}

	void Update()
	{
		for (auto& c : components) c->Update();
	}

	void Draw() 
	{
		for (auto& c : components) c->Draw();
	}

	void HandleEvent()
	{
		for (auto& c : components) c->HandleEvent();
	}

	bool isActive() const { return active; }
	void Destroy() { active = false; }

	bool HasGroup(Group mGroup)
	{
		return groupBitSet[mGroup];
	}

	void AddToGroup(Group mGroup);
	void RemoveFromGroup(Group mGroup)
	{
		groupBitSet[mGroup] = false;
	}

	template <typename T> bool HasComponent() const
	{
		return componentBitSet[getComponentTypeID<T>()];
	}

	template <typename T, typename... Targs> T& AddComponent(Targs&&... mArgs)
	{
		T* c(new T(std::forward<Targs>(mArgs)...));
		c->entity = this;
		std::unique_ptr<Component> uPtr{ c };
		components.emplace_back(std::move(uPtr));

		componentArray[getComponentTypeID<T>()] = c;
		componentBitSet[getComponentTypeID<T>()] = true;

		c->Init();

		return *c;
	}

	template <typename T> T& GetComponent() const
	{
		auto ptr(componentArray[getComponentTypeID<T>()]);
		return *static_cast<T*>(ptr);
	}

private:
	Manager& manager;
	bool active = true;
	std::vector<std::unique_ptr<Component>> components;
	ComponentArray componentArray;
	ComponentBitSet componentBitSet;
	GroupBitSet groupBitSet;
};


class Manager
{
private:
	std::vector<std::unique_ptr<Entity>> entities;
	std::array<std::vector<Entity*>, maxGroups> groupedEntities;

public:
	void Update()
	{
		for (auto& e : entities) e->Update();
	}

	void Draw()
	{
		for (auto& e : entities) e->Draw();
	}

	void HandleEvent()
	{
		for (auto& e : entities) e->HandleEvent();
	}

	void Refresh()
	{
		for (auto i(0u); i < maxGroups; i++)
		{
			auto& v(groupedEntities[i]);
			v.erase(std::remove_if(std::begin(v), std::end(v), [i](Entity* mEntity)
			{
				return !mEntity->isActive() || !mEntity->HasGroup(i);
			}), std::end(v));
		}

		entities.erase(std::remove_if(std::begin(entities), std::end(entities), [](const std::unique_ptr<Entity> &mEntity)
		{
			return !mEntity->isActive();
		}),
			std::end(entities));
	}

	void AddToGroup(Entity* entity, Group group)
	{
		groupedEntities[group].emplace_back(entity);
	}

	std::vector<Entity*>& GetGroup(Group group)
	{
		return groupedEntities[group];
	}

	Entity& AddEntity()
	{
		Entity* e = new Entity(*this);
		std::unique_ptr<Entity> uPtr{ e };
		entities.emplace_back(std::move(uPtr));
		return *e;
	}
};