#pragma once

#include "SDL.h"
#include "Map.h"
#include "SDL_image.h"
#include <iostream>

class ColliderComponent;
class MouseController;

class Game
{
public:
	Game();
	~Game();

	void Init(const char* title, int width, int height, bool fullscreen);

	void HandleEvents();
	void Update();
	void Render();
	void Clean();

	enum GroupLabels : std::size_t 
	{
		groupMap,
		groupRooms,
		groupPlayers,
		groupParticles,
		groupUI
	};

	static SDL_Renderer* renderer;
	static SDL_Event event;
	static Map* map;
	static bool isRunning;
	static SDL_Rect camera;
	static int SCALE;

private:
	SDL_Window *window;
};