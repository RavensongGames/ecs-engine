#pragma once

#include "SDL.h"

class TileComponent : public Component
{
public:
	SDL_Texture* texture;
	SDL_Rect srcRect, destRect;
	Vector2D position;

	TileComponent() = default;
	TileComponent(int srcX, int srcY, int x, int y, int tSize, const char* path)
	{
		texture = TextureManager::LoadTexture(path);

		position.x = (float)x;
		position.y = (float)y;

		srcRect.x = srcX;
		srcRect.y = srcY;
		srcRect.w = srcRect.h = tSize;

		destRect.x = x;
		destRect.y = y;
		destRect.w = destRect.h = tSize * Game::SCALE;
	}

	~TileComponent()
	{
		SDL_DestroyTexture(texture);
	}

	void Update() override
	{
		destRect.x = static_cast<int>(position.x) - Game::camera.x;
		destRect.y = static_cast<int>(position.y) - Game::camera.y;
	}

	void Draw() override
	{
		TextureManager::Draw(texture, srcRect, destRect, SDL_FLIP_NONE);
	}
};
