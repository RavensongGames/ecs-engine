#pragma once

#include "Vector2D.h"

class TransformComponent : public Component
{
public:
	Vector2D position;

	int height = 32;
	int width = 32;
	int scale = 1;

	TransformComponent()
	{
		position.Zero();
	}

	TransformComponent(float x, float y)
	{
		position.x = x;
		position.y = y;
	}

	TransformComponent(float x, float y, int w, int h, int sc)
	{
		position.x = x;
		position.y = y;
		width = w;
		height = h;
		scale = sc;
	}

	TransformComponent(Vector2D pos, int w, int h, int sc)
	{
		position = pos;
		width = w;
		height = h;
		scale = sc;
	}
};
