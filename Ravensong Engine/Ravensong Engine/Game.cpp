#include "Game.h"
#include "TextureManager.h"
#include "ECS.h"
#include "Components.h"

SDL_Renderer* Game::renderer = nullptr;
SDL_Rect Game::camera = { 0, 0, 0, 0 };
bool Game::isRunning = false;
int Game::SCALE = 3;
SDL_Event Game::event;

Manager manager;
Map* Game::map;

auto& controller(manager.AddEntity());
auto& tiles(manager.GetGroup(Game::groupMap));
auto& rooms(manager.GetGroup(Game::groupRooms));
auto& creatures(manager.GetGroup(Game::groupPlayers));

Game::Game(){}
Game::~Game(){}

void Game::Init(const char* title, int width, int height, bool fullscreen)
{
	int flags = (fullscreen) ? SDL_WINDOW_FULLSCREEN : 0;

	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags);
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if (renderer)
			SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		else
			std::cout << "No Renderer." << std::endl;

		isRunning = true;
	}
	else
	{
		isRunning = false;
		std::cout << "Could not initialize SDL." << std::endl;
	}

	map = new Map("Assets/terrain_ss.png", SCALE, 32);
	map->LoadMap("Assets/map.map");

	Game::camera.w = width;
	Game::camera.h = height;
	Game::camera.x = map->maxX() / 2;
	Game::camera.y = map->maxY() / 2;

	controller.AddComponent<MouseController>();
	controller.AddComponent<KeyboardController>();
}

void Game::HandleEvents()
{
	while (SDL_PollEvent(&event))
	{
		manager.HandleEvent();

		switch (event.type)
		{
		case SDL_QUIT:
			isRunning = false;
			break;
		default:
			break;
		}
	}

	if (camera.x > map->maxX() - camera.w)
		camera.x = map->maxX() - camera.w;
	if (camera.y > map->maxY() - camera.h)
		camera.y = map->maxY() - camera.h;

	if (camera.x < 0)
		camera.x = 0;
	if (camera.y < 0)
		camera.y = 0;
}

void Game::Update()
{
	manager.Refresh();
	manager.Update();
}

void Game::Render()
{
	SDL_RenderClear(renderer);

	for (auto& t : tiles)
	{
		t->Draw();
	}

	for (auto& c : creatures)
	{
		c->Draw();
	}

	SDL_RenderPresent(renderer);
}

void Game::Clean()
{
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
}