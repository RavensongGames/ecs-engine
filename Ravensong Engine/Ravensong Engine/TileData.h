#pragma once

#include "RoomComponent.h"
#include <vector>

class TileData
{
public:
	TileData() {
		posX = 0;
		posY = 0;
	}

	TileData(int x, int y)
	{
		posX = x;
		posY = y;
	}

	void SetRoom(RoomComponent* rm)
	{
		room = rm;
	}

	void SetNeighbors(TileData* td[50][30])
	{
		for (int x = -1; x <= 1; x++)
		{
			for (int y = -1; y <= 1; y++)
			{
				if (x == 0 && y == 0)
					continue;

				int cx = posX + x;
				int cy = posY + y;

				if (cx < 0 || cy < 0 || cx >= 50 || cy >= 30)
					continue;

				neighbors.push_back(td[cx][cy]);
			}
		}
	}

	bool hasRoom()
	{
		return room != nullptr;
	}

protected:
	int posX;
	int posY;

private:
	RoomComponent* room = nullptr;
	std::vector<TileData*> neighbors;
};