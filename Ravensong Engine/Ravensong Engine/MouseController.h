#pragma once

#include "Vector2D.h"
#include "ECS.h"
#include "Components.h"

extern Manager manager;

class MouseController : public Component
{
public:
	void Init() override
	{
		leftDown = false;
		rightDown = false;
		lastMousePos = Vector2D(0, 0);
		mousePosition = Vector2D(0, 0);
	}

	void HandleEvent() override
	{
		switch (Game::event.type)
		{
		case (SDL_MOUSEBUTTONDOWN):
			if (Game::event.button.button == SDL_BUTTON_LEFT)
				leftDown = true;
			if (Game::event.button.button == SDL_BUTTON_RIGHT) 
				rightDown = true;
			break;
		case (SDL_MOUSEBUTTONUP):
			if (Game::event.button.button == SDL_BUTTON_LEFT)
			{
				Click();
				leftDown = false;
			}
				
			if (Game::event.button.button == SDL_BUTTON_RIGHT)
				rightDown = false;
			break;
		case (SDL_MOUSEMOTION):
			mousePosition.x = (float)Game::event.motion.x;
			mousePosition.y = (float)Game::event.motion.y;
			break;
		default:
			break;
		}

		MouseScrolling();
	}

	void MouseScrolling()
	{
		if (rightDown)
		{
			int moveAmountX = static_cast<int>(mousePosition.x - lastMousePos.x);
			int moveAmountY = static_cast<int>(mousePosition.y - lastMousePos.y);

			Game::camera.x -= moveAmountX;
			Game::camera.y -= moveAmountY;

			lastMousePos.x = (float)Game::event.motion.x;
			lastMousePos.y = (float)Game::event.motion.y;
		}
		else
		{
			lastMousePos = mousePosition;
		}
	}

	void Click()
	{
		int tx = TileX();
		int ty = TileY();
		if (!Game::map->RoomAt(tx, ty))
		{
			auto& room(manager.AddEntity());
			RoomComponent* rmC = &room.AddComponent<RoomComponent>(tx, ty, 1, 1);

			room.AddComponent<TransformComponent>((float)(tx * (32 * Game::SCALE)), (float)(ty * (32 * Game::SCALE)), 32 * rmC->width, 32 * rmC->height, Game::SCALE);
			room.AddComponent<SpriteComponent>("Assets/room.png");
			room.AddToGroup(Game::groupPlayers);


			for (int x = rmC->posX; x < rmC->posX + rmC->width; x++)
			{
				for (int y = rmC->posY; y < rmC->posY + rmC->height; y++)
				{
					Game::map->GetTileAt(x, y)->SetRoom(rmC);
				}
			}
		}
	}

	Vector2D GetMousePosition()
	{
		return mousePosition;
	}

	int TileX()
	{
		return static_cast<int>((mousePosition.x + Game::camera.x) / (32 * Game::SCALE));
	}

	int TileY()
	{
		return static_cast<int>((mousePosition.y + Game::camera.y) / (32 * Game::SCALE));
	}

private:
	bool leftDown;
	bool rightDown;
	Vector2D mousePosition;
	Vector2D lastMousePos;
};
