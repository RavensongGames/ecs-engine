#pragma once

#include "ECS.h"

struct RoomComponent : public Component
{
	RoomComponent(int x, int y, int w, int h) {
		posX = x;
		posY = y;
		width = w;
		height = h;
	}

	int posX;
	int posY;
	int width;
	int height;
};