#include "ECS.h"

void Entity::AddToGroup(Group group)
{
	groupBitSet[group] = true;
	manager.AddToGroup(this, group);
}